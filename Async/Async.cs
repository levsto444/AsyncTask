﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class Async
{
    public static void Main()
    {
        CallMethod();
    }

    public static async void CallMethod()
    {
        Task<int[]> task1 = MakeArray();

        Console.WriteLine(string.Join(",", task1.Result));

        Task<int[]> task2 = MultiplyArray(await task1);

        Console.WriteLine(string.Join(",", task2.Result));

        Task<int[]> task3 = SortArray(await task2);

        Console.WriteLine(string.Join(",", task3.Result));

        Task<double> task4 = Average(await task3);

        Console.WriteLine(string.Join(",", task4.Result));
    }

    public static async Task<int[]> MakeArray()
    {
        int Min = 0;
        int Max = 45;

        int[] arr = new int[23];

        Random randNum = new();
        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = randNum.Next(Min, Max);
            }
        });

        return arr;
    }

    public static async Task<int[]> MultiplyArray(int[] arr)
    {
        int Min = 0;
        int Max = 5;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; ++i)
                arr[i] *= multiplier;
        });

        return arr;
    }

    public static Task<int[]> SortArray(int[] arr)
    {
        return Task.FromResult(arr.OrderBy(x => x).ToArray());
    }

    public static Task<double> Average(int[] arr)
    {
        return Task.FromResult(Queryable.Average(arr.AsQueryable()));
    }
}
